package dk.dtu.cee.prototypes.clock.interfaces;

import java.time.temporal.Temporal;

public interface Clock {
    Temporal getCurrentTime();

    Timer createTimer();
}
