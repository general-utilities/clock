package dk.dtu.cee.prototypes.clock.implementations.manual;

import dk.dtu.cee.prototypes.clock.interfaces.Timer;

import java.time.LocalDateTime;
import java.time.temporal.TemporalAmount;

public class ManualTimer implements Timer {

    private final ManualClock clock;

    public ManualTimer(final ManualClock clock) {
        this.clock = clock;
    }

    @Override
    public ManualClock getClock() {
        return clock;
    }

    @Override
    public void schedule(Runnable runnable, TemporalAmount delay) {
        LocalDateTime start = LocalDateTime.from(clock.getCurrentTime().plus(delay));
        TimerTask task = new TimerTask(this, runnable, start);

        clock.submitTask(task);
    }

    @Override
    public void scheduleAtFixedRate(Runnable runnable, TemporalAmount delay, TemporalAmount period) {
        LocalDateTime start = LocalDateTime.from(clock.getCurrentTime().plus(delay));
        TimerTask task = new TimerTask(this, runnable, start, period);

        clock.submitTask(task);
    }

    @Override
    public void cancel() {
        // TODO Loop through TreeSet (tasks) and cancel tasks associated with this timer
        // TODO Find more efficient way of looping through- and cancelling (perhaps a map?)
    }

    @Override
    public boolean isCanceled() {
        return false;
    }

    @Override
    public boolean allTasksDone() {
        return false;
    }


}
