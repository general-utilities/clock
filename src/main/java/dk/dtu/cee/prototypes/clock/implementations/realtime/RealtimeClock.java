package dk.dtu.cee.prototypes.clock.implementations.realtime;

import dk.dtu.cee.prototypes.clock.interfaces.Clock;

import java.time.LocalDateTime;
import java.time.temporal.Temporal;

public class RealtimeClock implements Clock {
    @Override
    public Temporal getCurrentTime() {
        return LocalDateTime.now();
    }

    @Override
    public RealtimeTimer createTimer() {
        return new RealtimeTimer(this);
    }

    public RealtimeTimer createTimer(int poolSize) {
        return new RealtimeTimer(this, poolSize);
    }
}
