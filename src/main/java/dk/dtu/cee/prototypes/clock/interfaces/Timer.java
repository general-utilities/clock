package dk.dtu.cee.prototypes.clock.interfaces;

import java.time.temporal.TemporalAmount;

public interface Timer {
    Clock getClock();

    void schedule(Runnable runnable, TemporalAmount delay);

    void scheduleAtFixedRate(Runnable runnable, TemporalAmount delay, TemporalAmount period);

    void cancel();
    boolean isCanceled();
    boolean allTasksDone();
}
