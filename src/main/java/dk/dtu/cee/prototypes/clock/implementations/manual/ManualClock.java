package dk.dtu.cee.prototypes.clock.implementations.manual;

import dk.dtu.cee.prototypes.clock.interfaces.Clock;

import java.time.LocalDateTime;
import java.time.temporal.Temporal;
import java.time.temporal.TemporalAmount;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

public class ManualClock implements Clock {

    private LocalDateTime time;
    private Set<ManualTimer> timers = new LinkedHashSet<>();

    private final TreeSet<TimerTask> tasks = new TreeSet<>();

    public ManualClock() {
        this(LocalDateTime.now());
    }

    public ManualClock(Temporal start) {
        this.time = LocalDateTime.from(start);
    }

    private void executeTo(LocalDateTime target) {
        synchronized (tasks) {
            synchronized (time) {
                while (tasks.size() > 0 && tasks.first().getStart().compareTo(target) <= 0) {
                    TimerTask task = tasks.first();
                    tasks.remove(task);

                    time = task.getStart(); // Update the time, in case the task calls the source clock

                    task.run();

                    TimerTask nextTask = task.nextTask();

                    if (nextTask != null) {
                        tasks.add(nextTask);
                    }
                }
            }
        }
    }

    void submitTask(final TimerTask task) {
        synchronized (tasks) {
            tasks.add(task);
        }
    }

    public void advance(TemporalAmount step) {
        synchronized (time) {
            LocalDateTime target = this.time.plus(step);    // Set target
            executeTo(target);                              // Execute tasks (which auto-updates time as it goes)
            this.time = target;                             // Set the time to to the original target
        }
    }

    @Override
    public Temporal getCurrentTime() {
        synchronized (time) {
            return this.time;
        }
    }

    @Override
    public ManualTimer createTimer() {
        synchronized (time) {
            ManualTimer timer = new ManualTimer(this);

            timers.add(timer);

            return timer;
        }
    }


}
