package dk.dtu.cee.prototypes.clock.implementations.manual;

import dk.dtu.cee.prototypes.clock.interfaces.Timer;

import java.time.LocalDateTime;
import java.time.temporal.TemporalAmount;

class TimerTask implements Runnable, Comparable<TimerTask> {
    private final Timer timer;
    private final Runnable runnable;
    private final LocalDateTime start;
    private final TemporalAmount period;

    public TimerTask(final Timer timer, final Runnable runnable, final LocalDateTime start) {
        this(timer, runnable, start, null);
    }

    public TimerTask(final Timer timer, final Runnable runnable, final LocalDateTime start, final TemporalAmount period) {
        this.timer = timer;
        this.runnable = runnable;
        this.start = start;
        this.period = period;
    }

    public TimerTask nextTask() {
        if (period == null)
            return null;

        TimerTask nextTask = new TimerTask(timer, runnable, start.plus(period), period);
        return nextTask;
    }

    public LocalDateTime getStart() {
        return start;
    }

    public TemporalAmount getPeriod() {
        return period;
    }

    @Override
    public void run() {
        this.runnable.run();
    }

    @Override
    public int compareTo(TimerTask task) {
        return start.compareTo(task.getStart());
    }
}
