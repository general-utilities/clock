
package dk.dtu.cee.prototypes.clock.implementations.realtime;

import dk.dtu.cee.prototypes.clock.interfaces.Timer;

import java.time.Duration;
import java.time.temporal.TemporalAmount;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class RealtimeTimer implements Timer {

    private final static int DEFAULT_POOL_SIZE = 2;

    private final ScheduledExecutorService executor;
    private final RealtimeClock clock;

    public RealtimeTimer(final RealtimeClock clock) {
        this(clock, DEFAULT_POOL_SIZE);
    }

    public RealtimeTimer(final RealtimeClock clock, int poolSize) {
        this.clock = clock;

        executor = Executors.newScheduledThreadPool(poolSize);
    }

    @Override
    public RealtimeClock getClock() {
        return clock;
    }

    @Override
    public void schedule(Runnable runnable, TemporalAmount delay) {
        executor.schedule(runnable, Duration.from(delay).toNanos(), TimeUnit.NANOSECONDS);
    }

    @Override
    public void scheduleAtFixedRate(Runnable runnable, TemporalAmount delay, TemporalAmount period) {
        executor.scheduleAtFixedRate(runnable, Duration.from(delay).toNanos(), Duration.from(period).toNanos(), TimeUnit.NANOSECONDS);
    }

    @Override
    public void cancel() {
        executor.shutdownNow();
    }

    @Override
    public boolean isCanceled() {
        return executor.isShutdown();
    }

    @Override
    public boolean allTasksDone() {
        return executor.isTerminated();
    }
}