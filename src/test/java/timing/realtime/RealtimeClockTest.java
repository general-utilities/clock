package timing.realtime;

import dk.dtu.cee.prototypes.clock.implementations.realtime.RealtimeClock;
import dk.dtu.cee.prototypes.clock.implementations.realtime.RealtimeTimer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;

public class RealtimeClockTest {

    private RealtimeClock clock;

    @Before
    public void init() {
        clock = new RealtimeClock();
    }

    @Test
    public void createTimer() {
        RealtimeTimer timer = clock.createTimer();

        Assert.assertEquals(clock, timer.getClock());
    }

    @Test
    public void timeLapse() {
        Temporal before = clock.getCurrentTime();

        try {
            Thread.sleep(RealtimeConstants.TIMER_SINGLE_DELAY);
        } catch (InterruptedException e) {
            Assert.fail("Wait was interrupted");
        }

        Temporal after = clock.getCurrentTime();

        long lapse = before.until(after, ChronoUnit.MILLIS);
        long diff = Math.abs(lapse- RealtimeConstants.TIMER_SINGLE_DELAY);

        Assert.assertEquals(String.format("Diff is too great: %dms", diff), true, diff < 15);
    }
}
