package timing.realtime;

public class RealtimeConstants {
    public static final long TIMER_PRECISION_MILLIS = 85;
    public static final long TIMER_SINGLE_DELAY = 2000;

    public static final int TIMER_REPEAT_ITERS = 3;
    public static final long TIMER_REPEAT_DELAY = 1000;
}
