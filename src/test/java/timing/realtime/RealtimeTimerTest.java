package timing.realtime;

import dk.dtu.cee.prototypes.clock.implementations.realtime.RealtimeClock;
import dk.dtu.cee.prototypes.clock.interfaces.Timer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.Duration;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class RealtimeTimerTest {

    private RealtimeClock clock;

    @Before
    public void init() {
        clock = new RealtimeClock();
    }

    @Test
    public void singleSchedule() {
        final long TIMER_SINGLE_TIMEOUT = RealtimeConstants.TIMER_SINGLE_DELAY + 2000;

        final Timer timer = clock.createTimer();
        final AtomicLong stop = new AtomicLong(-1);

        Object obj = clock.getClass().getName();

        long start = System.currentTimeMillis();
        timer.schedule(() -> {
            stop.set(System.currentTimeMillis());
            obj.notify();
        }, Duration.ofMillis(RealtimeConstants.TIMER_SINGLE_DELAY));

        try {
            synchronized (obj) {
                obj.wait(TIMER_SINGLE_TIMEOUT);
            }

            if (stop.get() > 0) {
                long lapse = stop.get() - start;
                long diff = Math.abs(lapse - RealtimeConstants.TIMER_SINGLE_DELAY);

                Assert.assertEquals(true, diff <= RealtimeConstants.TIMER_PRECISION_MILLIS);

            } else {
                Assert.fail("Timer did not executeTo before timeout");
            }

        } catch (InterruptedException e) {
            Assert.fail("Timer interrupted");
        }
    }

    @Test
    public void repeatedSchedule() {
        final long TIMER_REPEAT_TIMEOUT = RealtimeConstants.TIMER_REPEAT_DELAY * RealtimeConstants.TIMER_REPEAT_ITERS + 2000;

        final Timer timer = clock.createTimer();
        final List<Long> t = new LinkedList<>();
        final AtomicInteger iters = new AtomicInteger(RealtimeConstants.TIMER_REPEAT_ITERS);

        Object obj = clock.getClass().getName();

        t.add(System.currentTimeMillis());

        timer.scheduleAtFixedRate(() -> {
            t.add(System.currentTimeMillis());
            if (iters.decrementAndGet() == 0) {
                obj.notify();
            }
        }, Duration.ofMillis(RealtimeConstants.TIMER_REPEAT_DELAY), Duration.ofMillis(RealtimeConstants.TIMER_REPEAT_DELAY));

        try {
            synchronized (obj) {
                obj.wait(TIMER_REPEAT_TIMEOUT);
            }

            if (iters.get() == 0) {


                Iterator<Long> iter = t.iterator();
                Long previous = iter.hasNext() ? iter.next() : null;
                while (iter.hasNext()) {
                    final Long current = iter.next();

                    long lapse = Math.abs(previous - current);
                    long diff = Math.abs(lapse - RealtimeConstants.TIMER_REPEAT_DELAY);

                    Assert.assertEquals(true, diff <= RealtimeConstants.TIMER_PRECISION_MILLIS);

                    previous = current;
                }

            } else {
                Assert.fail("Timer did not executeTo before timeout");
            }

        } catch (InterruptedException e) {
            Assert.fail("Timer interrupted");
        }
    }
}
