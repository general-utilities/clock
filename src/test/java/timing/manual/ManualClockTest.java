package timing.manual;

import dk.dtu.cee.prototypes.clock.implementations.manual.ManualClock;
import dk.dtu.cee.prototypes.clock.implementations.manual.ManualTimer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class ManualClockTest {

    private ManualClock clock;

    @Before
    public void init() {
        clock = new ManualClock();
    }

    @Test
    public void createTimer() {
        ManualTimer timer = clock.createTimer();

        Assert.assertEquals(clock, timer.getClock());
    }

    @Test
    public void timeLapse() {
        long start = LocalDateTime.from(clock.getCurrentTime()).toInstant(ZoneOffset.UTC).toEpochMilli();

        clock.advance(Duration.ofMillis(ManualConstants.TIMER_ADVANCE_MILLIS));

        long finish = LocalDateTime.from(clock.getCurrentTime()).toInstant(ZoneOffset.UTC).toEpochMilli();

        long lapse = finish - start;

        Assert.assertEquals(ManualConstants.TIMER_ADVANCE_MILLIS, lapse);
    }
}
