package timing.manual;

import dk.dtu.cee.prototypes.clock.implementations.manual.ManualClock;
import dk.dtu.cee.prototypes.clock.interfaces.Timer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.Temporal;
import java.time.temporal.TemporalAmount;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;

public class ManualTimerTest {



    private ManualClock clock;

    @Before
    public void init() {
        clock = new ManualClock();
    }

    @Test
    public void singleSchedule() {
        Timer timer = clock.createTimer();

        List<Temporal> executionTimes = new ArrayList<>();
        Runnable runnable = mock(Runnable.class);

        doAnswer(invocation -> {
            executionTimes.add(clock.getCurrentTime());
            return null;
        }).when(runnable).run();

        timer.schedule(runnable, Duration.ofSeconds(ManualConstants.TIMER_DELAY_SEC));

        clock.advance(Duration.ofMinutes(ManualConstants.TIMER_ADVANCE_MINUTES));

        Assert.assertEquals(1, executionTimes.size());
    }

    @Test
    public void repeatedSchedule() {
        Timer timer = clock.createTimer();

        List<Temporal> executionTimes = new ArrayList<>();
        Runnable runnable = mock(Runnable.class);

        doAnswer(invocation -> {
            executionTimes.add(clock.getCurrentTime());
            return null;
        }).when(runnable).run();

        TemporalAmount delay = Duration.ofSeconds(ManualConstants.TIMER_DELAY_SEC);
        TemporalAmount period = Duration.ofSeconds(ManualConstants.TIMER_PERIOD_SEC);

        long start = LocalDateTime.from(delay.addTo(clock.getCurrentTime())).toInstant(ZoneOffset.UTC).toEpochMilli();

        timer.scheduleAtFixedRate(runnable, delay, period);

        clock.advance(Duration.ofHours(ManualConstants.TIMER_ADVANCE_HOURS));

        long finish = LocalDateTime.from(timer.getClock().getCurrentTime()).toInstant(ZoneOffset.UTC).toEpochMilli();

        long diff = finish - start;
        long lapse = Duration.from(period).toMillis();
        long iterations = (diff / lapse) + 1;

        Assert.assertEquals("Iteration count mismatch", iterations, executionTimes.size());
    }
}
