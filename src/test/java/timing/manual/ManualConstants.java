package timing.manual;

public class ManualConstants {
    public static final long TIMER_DELAY_SEC = 23;
    public static final long TIMER_PERIOD_SEC = 2;

    public static final long TIMER_ADVANCE_MILLIS = 5000;
    public static final long TIMER_ADVANCE_MINUTES = 4;
    public static final long TIMER_ADVANCE_HOURS = 48;
}
