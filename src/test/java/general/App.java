package general;

import dk.dtu.cee.prototypes.clock.implementations.realtime.RealtimeClock;
import dk.dtu.cee.prototypes.clock.interfaces.Clock;
import dk.dtu.cee.prototypes.clock.interfaces.Timer;

import java.io.IOException;
import java.time.Duration;
import java.time.temporal.Temporal;

public class App {
    public static void main(String...args) throws IOException {

        Clock rtClock = new RealtimeClock();
        Timer rtTimer = rtClock.createTimer();

        Temporal time = rtClock.getCurrentTime();
        System.out.printf("The current time is: %s\n", time);

        rtTimer.schedule(() -> System.out.println("Single timer elapsed"), Duration.ofSeconds(5));

        rtTimer.scheduleAtFixedRate(() -> System.out.println("Repeated timer elapsed"), Duration.ofSeconds(0), Duration.ofSeconds(2));


//        Clock mClock = new ManualClock();
//        Timer mTimer = mClock.createTimer();


        System.out.println("Press [enter] to terminate");
        System.in.read();
        System.out.println("Terminated");

        rtTimer.cancel();
    }
}
